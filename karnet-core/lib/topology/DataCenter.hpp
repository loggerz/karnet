//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_DATACENTER_HPP
#define KARNET_DATACENTER_HPP


#include <vector>
#include "DataNode.hpp"
#include "MasterNode.hpp"

class DataCenter
{
public:

private:
  std::vector<MasterNode> _masterNodes;
  std::vector<DataNode> _disks;
};


#endif//KARNET_DATACENTER_HPP
