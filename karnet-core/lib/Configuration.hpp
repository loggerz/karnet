//
// Created by loggerz on 23/08/2021.
//

#ifndef KARNET_CONFIGURATION_HPP
#define KARNET_CONFIGURATION_HPP

#include <string>
#include <filesystem>
#include <spdlog/logger.h>
#include <json/json.h>

#include "topology/Topology.hpp"
#include "Constants.hpp"

/**
 * Thread-safe Karnet configuration. Some parts of the configuration are mutable,
 * others requires a restart of the process to be updated. Configuration is read
 * once from the file and when a mutable field is updated, it is overwritten.
 */
class Configuration
{
public:
  explicit Configuration(std::filesystem::path jsonFilePath);

  // Accessors
  bool                         isValid() const { return _valid; }
  const std::string &          nodeName() const { return _nodeName; }
  KarnetRole                   role() const { return _role; }
  const std::filesystem::path &jsonFilePath() const { return _jsonFilePath; }
  const NamePathMap &disks() const { return _volumesRoot; }

  // Static helper methods
  static KarnetRole            stringToKarnetRole(const std::string &s);
  static std::string           karnetRoleToString(const KarnetRole &role);

private:
  std::shared_ptr<spdlog::logger> _log;
  const std::filesystem::path     _jsonFilePath;
  bool                            _valid;

  /* -- Common configuration --  */
  std::string                     _nodeName;
  KarnetRole                      _role;

  /* -- Storage configuration -- */
  NamePathMap                     _volumesRoot;

  void                            reset();
  bool                            writeToFile();
  bool                            loadFromJSON(const Json::Value &jsonRoot);
  Json::Value                     saveToJSON() const;
};


#endif//KARNET_CONFIGURATION_HPP
