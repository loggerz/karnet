//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_CONSTANTS_HPP
#define KARNET_CONSTANTS_HPP

#include <cstddef>
#include <filesystem>
#include <bitset>
#include <unordered_map>
#include <json/json.h>
#include <cryptopp/rsa.h>

/* Security */
using IdentityId                                            = uint32_t;

/* Volumes */
using VolumeId                                              = uint32_t;
using VolumeGroupId                                         = uint32_t;
using VolumeFlagsBytes                                      = uint32_t;
using DataProtectionIndex                                   = uint8_t;// MSB : file type (0 : m / 1 : k) | 7 bits unsigned int : file index /or/ # of replication

/* Needles */
using Offset                                                = uint64_t;
using NeedleSize                                            = uint32_t;// cf MAX_NEEDLE_SIZE
using FileId                                                = uint64_t;
using NeedleCookie                                          = uint32_t;
using NeedleId                                              = std::array<uint32_t, 4>;// 128 bits = 32 bits VID + 64 bits FID + 32 bits cookie
using NeedleFlagsByte                                       = uint8_t;                // cf NeedleFlags
using NeedleHash                                            = std::array<uint32_t, 4>;// 128 bits for MD5

/* Configuration */
using NamePathMap                                           = std::unordered_map<std::string, std::unique_ptr<std::filesystem::path>>;

static constexpr std::string_view FILE_EXTENSION_VOLUME     = ".kvol";

static constexpr size_t           MAGIC_NUMBERS_LENGTH      = 8;
static constexpr std::string_view MAGIC_NUMBER_FILE         = "KAR001FF";
static constexpr std::string_view MAGIC_NUMBER_NEEDLE_START = "KAR001NS";
static constexpr std::string_view MAGIC_NUMBER_NEEDLE_END   = "KAR001NE";
static constexpr std::string_view MAGIC_NUMBER_INDEX        = "KAR001II";
static constexpr std::string_view MAGIC_NUMBER_ENTRY        = "KAR001EE";

static constexpr uint32_t         VOLUME_HEADER_SIZE        = 8 * 1024;            // 8 KiB
static constexpr uint32_t         INDEX_HEADER_SIZE         = 1024;                // 1 KiB
static constexpr uint32_t         MAX_NEEDLE_SIZE           = UINT32_MAX;          // 4 GiB -1
static constexpr Offset           MAX_VOLUME_SIZE           = 8L * MAX_NEEDLE_SIZE;// 32 GiB

static constexpr std::streamoff   NEEDLE_MAX_FOOTER_SIZE    = 32;
static constexpr std::streamoff   NEEDLE_CHECKSUM_SIZE      = 16;
static constexpr Offset           NEEDLE_OVERHEAD           = MAGIC_NUMBER_NEEDLE_START.size() * 2 + sizeof(FileId) + sizeof(NeedleCookie) + sizeof(NeedleSize) + sizeof(NeedleFlagsByte) + NEEDLE_CHECKSUM_SIZE;
static constexpr uint32_t         BYTES_ALIGNMENT           = 8;
static constexpr Offset           INDEX_PARSING_CHUNK_SIZE  = 10L * 1024 * 1024;

static constexpr uint8_t          COMPUTE_NEEDLE_PADDING(NeedleSize dataSize) {
  return BYTES_ALIGNMENT - dataSize % BYTES_ALIGNMENT - 1;
}
static constexpr uint64_t NEEDLE_TOTAL_SPACE(NeedleSize dataSize) {
  return NEEDLE_OVERHEAD + dataSize + COMPUTE_NEEDLE_PADDING(dataSize);
}


/**
 * Disk type ordered by latency and speed
 */
enum class DiskType {
  RAM     = 5,
  NVMe    = 4,
  SSD     = 3,
  HDD     = 2,
  Unknown = 1,
  Archive = 0
};

enum class SecurityLevel {
  PUBLIC,
  PRIVATE,
  CONFIDENTIAL,
  BLACK
};

enum class VotingRequirements {
  DISABLED,
  ANY_MEMBER,
  MAJORITY_OF_MEMBERS,
  ALL_MEMBERS
};

enum class TTL {
  TTL_3m,
  TTL_4h,
  TTL_1d,
  TTL_2w,
  TTL_1M,
  TTL_6M,
  TTL_1y
};

enum class NeedleFlags {
  None    = UINT8_C(0),
  Deleted = UINT8_C(0b1),
  Incomplete = UINT8_C(0b01)
};

enum class VolumeFlags : VolumeFlagsBytes {
  None           = UINT32_C(0),
  Sealed         = UINT32_C(1),
  AppendOnly     = UINT32_C(1U << 1),
  DataProtection = UINT32_C(1U << 2)// 0 : replication, 1 : erasure coding
};

#endif// KARNET_CONSTANTS_HPP
