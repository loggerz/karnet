//
// Created by loggerz on 23/08/2021.
//

#include <spdlog/sinks/stdout_color_sinks.h>
#include <utility>
#include <fstream>

#include "Configuration.hpp"

Configuration::Configuration(std::filesystem::path jsonFilePath)
  : _log(spdlog::stdout_color_mt("Configuration")), _jsonFilePath(std::move(jsonFilePath)), _valid(false), _role(KarnetRole::Unknown) {
  _log->set_level(spdlog::level::debug);

  if (!std::filesystem::exists(_jsonFilePath)) {
    _log->info("No config.json file, setting up default configuration ... Please edit the file at {}", _jsonFilePath.string());
    reset();
    writeToFile();
    return;
  }

  std::ifstream configFileStream(_jsonFilePath);
  Json::Value   jsonRoot;

  if (!configFileStream.is_open()) {
    _log->critical("Failed to open {}", _jsonFilePath.string());
    return;
  }

  configFileStream >> jsonRoot;
  configFileStream.close();

  _valid = loadFromJSON(jsonRoot);
}

KarnetRole Configuration::stringToKarnetRole(const std::string &s) {
  if (s == "client") {
    return KarnetRole::Client;
  }
  if (s == "workstation") {
    return KarnetRole::Workstation;
  }
  if (s == "eg") {
    return KarnetRole::InternalGateway;
  }
  if (s == "ig") {
    return KarnetRole::InternalGateway;
  }
  if (s == "storage") {
    return KarnetRole::StorageNode;
  }
  return KarnetRole::Unknown;
}

std::string Configuration::karnetRoleToString(const KarnetRole &role) {
  switch (role) {
  case KarnetRole::Client: return "client";
  case KarnetRole::Workstation: return "workstation";
  case KarnetRole::ExternalGateway: return "eg";
  case KarnetRole::InternalGateway: return "ig";
  case KarnetRole::StorageNode: return "storage";
  default: return "unknown";
  }
}

bool Configuration::writeToFile() {
  std::ofstream configFileStream(_jsonFilePath);
  if (!configFileStream.is_open()) {
    _log->critical("Could not write configuration to config.json");
    return false;
  }

  auto config = saveToJSON();
  configFileStream << config;
  configFileStream.close();
  return true;
}

void Configuration::reset() {
  /* Common configuration */
  _nodeName    = "defaultNode";
  _role        = KarnetRole::Unknown;

  /* Storage configuration */
  _volumesRoot = NamePathMap();
//  _volumesRoot.insert({ "tmp", std::make_unique<std::filesystem::path>(std::filesystem::path{ "/tmp/karnet_volumes" }) });
}

bool Configuration::loadFromJSON(const Json::Value &jsonRoot) {
  if (!jsonRoot["common"].isObject() || !jsonRoot["storage"].isObject()) {
    _log->critical("Missing a config section (common, storage)");
    return false;
  }

  /* Common configuration */
  auto commonConfig = jsonRoot["common"];
  if (!commonConfig["node_name"].isString() || !commonConfig["role"].isString()) {
    _log->critical("incomplete common section");
    return false;
  }
  _nodeName          = commonConfig["node_name"].asString();
  _role              = stringToKarnetRole(commonConfig["role"].asString());

  /* Storage configuration */
  auto storageConfig = jsonRoot["storage"];

  auto jsonDisksRoot = storageConfig["volumes_root"];
  if (!jsonDisksRoot.isArray()) {
    _log->critical("volumes_root should be an array in storage");
    return false;
  }
  if (jsonDisksRoot.empty()) {
    _log->critical("No volume root specified in config.json ! Edit the configuration to add one");
    return false;
  }
  return !std::any_of(jsonDisksRoot.begin(),  jsonDisksRoot.end(), [this](Json::Value jsonDiskRoot){
    if (!jsonDiskRoot.isObject()
        || !jsonDiskRoot["name"].isString()
        || !jsonDiskRoot["path"].isString()) {
      _log->critical("An element of volumes_root is not an object{name:string,path:string}");
      return true;
    }
    const std::string           name{ jsonDiskRoot["name"].asString() };
    const std::filesystem::path path{ jsonDiskRoot["path"].asString() };
    if (!path.is_absolute()) {
      _log->critical("Path {} is not absolute", path.c_str());
      return true;
    }
    _volumesRoot = NamePathMap{};
    _volumesRoot.insert({ name,
      std::make_unique<std::filesystem::path>(path) });
    return false;
  });
}

Json::Value Configuration::saveToJSON() const {
  Json::Value jsonRoot;

  /* Common Configuration */
  Json::Value commonConfig;
  commonConfig["node_name"] = _nodeName;
  commonConfig["role"]      = karnetRoleToString(_role);
  jsonRoot["common"]        = std::move(commonConfig);

  /* Storage Configuration */
  Json::Value storageConfig;

  Json::Value volumesRootJson{ Json::arrayValue };
  for (const auto &itr : _volumesRoot) {
    Json::Value volumeRoot;
    volumeRoot["name"] = itr.first;
    volumeRoot["path"] = itr.second->c_str();
    volumesRootJson.append(std::move(volumeRoot));
  }
  storageConfig["volumes_root"] = std::move(volumesRootJson);
  jsonRoot["storage"]           = std::move(storageConfig);

  return jsonRoot;
}