//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_LOGGER_HPP
#define KARNET_LOGGER_HPP

#include <spdlog/spdlog.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>

class Logger
{
public:

private:
  std::shared_ptr<spdlog::logger> _log;
};


#endif//KARNET_LOGGER_HPP
