# Karnet

Storage system inspired by Facebook's Haystack. Documentation and general info is WIP.

### Documentation

To generate the documentation, just type `doxygen` at the project root.


### Compilation dependencies

- Optional : ccache
- cmake
- conan
- CryptoPP / Crypto++ (with its CMake files ! `cmake --build . --target install`)
- qt6-base
- doxygen
- cppcheck
- clang-tidy (clang ?)

# Long-term TODO List

- LocalVolume::parseIndexFile() : read a whole 1MiB of data at a time in a buffer then parse it. -> Faster file parsing