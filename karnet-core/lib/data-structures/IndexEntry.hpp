//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_INDEXENTRY_HPP
#define KARNET_INDEXENTRY_HPP

#include <cstdint>
#include <unordered_map>
#include <memory>

#include "../Constants.hpp"

class IndexEntry
{
public:
  IndexEntry(FileId fid, NeedleCookie cookie, NeedleSize size, Offset offset, NeedleFlags flags)
    : _fid(fid), _cookie(cookie), _size(size), _offset(offset), _flags(flags) {}

  [[nodiscard]] FileId       fid() const { return _fid; }
  [[nodiscard]] NeedleCookie cookie() const { return _cookie; }
  [[nodiscard]] NeedleSize   size() const { return _size; }
  [[nodiscard]] Offset       offset() const { return _offset; }
  [[nodiscard]] NeedleFlags  flags() const { return _flags; }

private:
  FileId       _fid;   //8B
  NeedleCookie _cookie;//4B
  NeedleSize   _size;  //4B
  Offset       _offset;//8B
  NeedleFlags  _flags; //1B
  //7B of padding = total 32B
};

#endif//KARNET_INDEXENTRY_HPP
