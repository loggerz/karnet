add_executable(core_tests lib/storage/data-structures.tests.cpp lib/storage/local-storage.tests.cpp)
target_link_libraries(core_tests PRIVATE project_warnings project_options KarnetCore catch_main)
catch_discover_tests(core_tests)