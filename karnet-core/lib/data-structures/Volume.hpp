//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_VOLUME_HPP
#define KARNET_VOLUME_HPP

#include <spdlog/spdlog.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <fstream>
#include <optional>

#include "IndexEntry.hpp"
#include "../Constants.hpp"

/**
 * A general lightweight representation of a volume
 */
class Volume
{
public:
  explicit Volume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi);
  Volume();
  Volume(const Volume &) = default;
  Volume(Volume &&)      = default;
  Volume &operator=(const Volume &) = default;
  Volume &operator=(Volume &&) = default;
  virtual ~Volume()            = default;

  [[nodiscard]] virtual std::string string() const;

  [[nodiscard]] VolumeId            vid() const { return _vid; }
  [[nodiscard]] VolumeGroupId       vgid() const { return _vgid; }
  [[nodiscard]] VolumeFlags         flags() const { return _flags; }
  [[nodiscard]] DataProtectionIndex dpi() const { return _dpi; }

protected:
  [[nodiscard]] auto log() const { return _log; }

private:
  std::shared_ptr<spdlog::logger> _log;
  VolumeId                        _vid;
  VolumeGroupId                   _vgid;
  VolumeFlags                     _flags;
  DataProtectionIndex             _dpi;
};

/**
 * A representation of a volume specifically stored on the current host. This class is able to alter the volume file
 * TODO: THREAD SAFETY !!
 */
class LocalVolume : public Volume
{
public:
  LocalVolume()                    = delete;
  LocalVolume(const LocalVolume &) = delete;
  LocalVolume(LocalVolume &&)      = default;
  LocalVolume &operator=(const LocalVolume &) = delete;
  LocalVolume &operator=(LocalVolume &&) = default;
  ~LocalVolume() override;

  static LocalVolume              loadFromHeader(std::filesystem::path path);
  static LocalVolume              createNewVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi, std::filesystem::path volumeRootFolder);

  std::pair<FileId, NeedleCookie> createNeedle(NeedleSize size);

  std::string                     string() const override;
  bool                            hasEnoughSpace(NeedleSize spaceSize) const;
  Offset                          size() const { return _fileSize; }


private:
  std::basic_fstream<char>               _fs;
  std::filesystem::path                  _path;
  std::filesystem::path                  _indexPath;

  std::unordered_map<FileId, IndexEntry> _index;
  std::vector<FileId>                    _entriesToSave;
  Offset                                 _fileSize;

  explicit LocalVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi, std::basic_fstream<char> fs, std::filesystem::path path);
  explicit LocalVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi, std::filesystem::path path);
  bool                    writeHeader(std::optional<VolumeFlags> newFlags = {});
  std::pair<bool, Offset> appendNeedle(FileId fid, NeedleCookie cookie, NeedleSize size, NeedleFlags flags);
  bool                    savePendingIndexEntries();
  void                    parseIndexFile();
  void                    rebuildIndex();

  static bool             checkMagicNumber(const std::array<char, MAGIC_NUMBERS_LENGTH> &magicArr, const std::string_view &magicStr);
};


#endif// KARNET_VOLUME_HPP
