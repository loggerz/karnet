//
// Created by loggerz on 06/09/2021.
//

#include <iostream>
#include "CLIParser.hpp"

CLIParser::CLIParser(const std::map<std::basic_string<char>, docopt::value> &args, [[maybe_unused]] LocalStorage &ls) {

  if (args.at("volume").asBool()) {

    if (args.at("create").asBool() && !args.at("needle").asBool()) {
      ls.createVolume(0x1, static_cast<VolumeFlags>(0x2), 0x3);
    } else if (args.at("list").asBool()) {
      std::cout << "\nList of volumes on this host:\n";
      auto volPairs = ls.getAllLocalVolumeIds();
      for (const auto &volPair : volPairs) {
        std::cout << fmt::format("Disk {}, VID {:x}\n", volPair.second, volPair.first);
      }
      if (volPairs.empty()) {
        std::cout << "\nNo volume found\n";
      }
    } else {
      auto vidStr = args.at("<vid>").asString();
      try {
        auto        vid    = static_cast<VolumeId>(stoul(vidStr, nullptr, 16));
        const auto &volume = ls.findVolume(vid);

        if (args.at("info").asBool()) {
          std::cout << "\n" << volume.string() << "\n";
        } else if (args.at("create").asBool()) {
          auto needleSize = static_cast<NeedleSize>(stoul(args.at("<size>").asString()));
          auto [fid, cookie] = ls.reserveSpace(vid, needleSize);
          std::cout << fmt::format("\nNew needle created : FID: {:x}   -   Cookie: {:x}\n", fid, cookie);
        } else if (args.at("index").asBool()) {
          std::cout << fmt::format("\nIndex dump {:x}.idx :", vid);
        }
      } catch (const std::invalid_argument &) {
        std::cout << fmt::format("\nInvalid VID : {}\n", vidStr);
        return;
      } catch (const std::out_of_range &) {
        std::cout << fmt::format("\nVolume {} not found\n", vidStr);
        return;
      }
    }
  }


  else if (args.at("dev").asBool()) {
    if (args.at("<fn>").asString() == "sizeof") {
      std::cout << fmt::format("\nsizeof(IndexEntry)={}\n", sizeof(IndexEntry));
    }
  }
}
