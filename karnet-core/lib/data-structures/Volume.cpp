//
// Created by loggerz on 20/08/2021.
//

#include <spdlog/sinks/stdout_color_sinks.h>
#include <utility>
#include <vector>
#include <string>
#include <fmt/format.h>
#include <array>
#include <random>
#include <iostream>

#include "Volume.hpp"

/* ------------------------- VOLUME ------------------------- */

Volume::Volume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi)
  : _vid(vid), _vgid(vgid), _flags(flags), _dpi(dpi) {

  _log = spdlog::stdout_color_mt(fmt::format("Volume {:x}", _vid));
  _log->set_level(spdlog::level::debug);
}

std::string Volume::string() const {
  return fmt::format("Volume {:x} : vgid {:L} - Flags {:L} - DPI {:L}", vid(), vgid(), flags(), dpi());
}

Volume::Volume() : _vid(0), _vgid(0), _flags(VolumeFlags::None), _dpi(0) {
}


/* ------------------------- LOCAL VOLUME ------------------------- */

/**
 * Create a representation of a locally stored volume (used by DataNodes)
 * @param vid must be the same as the VolumeId in the file's header
 * @param path of the .kvol file
 * @throws ERR_MAGIC if the file doesn't start with the MAGIC_NUMBER_FILE
 */
LocalVolume LocalVolume::loadFromHeader(std::filesystem::path path) {
  auto log = spdlog::stdout_color_mt(fmt::format("Volume Loader : {}", path.filename().string()));
  log->set_level(spdlog::level::debug);

  if (!std::filesystem::exists(path) || std::filesystem::file_size(path) < VOLUME_HEADER_SIZE) {
    log->warn("Invalid path ({})", path.string());
    throw std::runtime_error("ERR_PATH");
  }

  std::basic_fstream<char>                   fs(path, std::ios::binary | std::ios::in | std::ios::out);

  std::array<char, MAGIC_NUMBER_FILE.size()> magicNumber{};
  fs.read(magicNumber.data(), MAGIC_NUMBER_FILE.size());
  if (!checkMagicNumber(magicNumber, MAGIC_NUMBER_FILE)) {
    log->critical("The file {} is not a volume file ! [ERR_MAGIC]");
    throw std::runtime_error("ERR_MAGIC");
  }
  magicNumber.fill('\x00');

  VolumeId vid = 0;
  fs.read(static_cast<char *>(static_cast<void *>(&vid)), sizeof(VolumeId));

  VolumeGroupId vgid = 0;
  fs.read(static_cast<char *>(static_cast<void *>(&vgid)), sizeof(VolumeGroupId));

  VolumeFlags flags = VolumeFlags::None;
  fs.read(static_cast<char *>(static_cast<void *>(&flags)), sizeof(VolumeFlagsBytes));

  DataProtectionIndex dpi = 0;
  fs.read(static_cast<char *>(static_cast<void *>(&dpi)), sizeof(DataProtectionIndex));

  fs.close();

  return LocalVolume(vid, vgid, flags, dpi, std::move(fs), std::move(path));
}

LocalVolume::~LocalVolume() {
  if (log() != nullptr && !_entriesToSave.empty()) {
    log()->debug("Saving {} pending IndexEntries...", _entriesToSave.size());
    if (!savePendingIndexEntries()) {
      log()->critical("Could not save pending IndexEntries !");
    }
  }
}

LocalVolume LocalVolume::createNewVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi, std::filesystem::path volumeRootFolder) {
  auto log = spdlog::stdout_color_mt(fmt::format("Volume Creator : Volume_{:x}", vid));
  log->set_level(spdlog::level::debug);

  auto path = volumeRootFolder.append(fmt::format("{:x}.kvol", vid));
  log->debug("New volume at {}", path.string());

  if (std::filesystem::exists(path)) {
    log->warn("Path already exists ! ({})", path.string());
    throw std::runtime_error("ERR_PATH");
  }

  LocalVolume lv(vid, vgid, flags, dpi, path);
  if (!lv.writeHeader()) {
    log->warn("Could not write headers to disk");
    throw std::runtime_error("ERR_HEADER_WRITE");
  }
  return lv;
}

LocalVolume::LocalVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi, std::basic_fstream<char> fs, std::filesystem::path path)
  : Volume(vid, vgid, flags, dpi), _fs(std::move(fs)), _path(std::move(path)), _indexPath(_path), _fileSize{ VOLUME_HEADER_SIZE } {
  _indexPath.replace_filename(fmt::format("{:x}.kidx", vid));
  try {
    parseIndexFile();
  } catch (const std::runtime_error&) { rebuildIndex(); }
}

LocalVolume::LocalVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi, std::filesystem::path path)
  : Volume(vid, vgid, flags, dpi), _path(std::move(path)), _indexPath(_path), _fileSize{ VOLUME_HEADER_SIZE } {
  _indexPath.replace_filename(fmt::format("{:x}.kidx", vid));
  try {
    parseIndexFile();
  } catch (const std::runtime_error&) { rebuildIndex(); }
}

bool LocalVolume::writeHeader(std::optional<VolumeFlags> newFlags) {
  log()->debug("Writing header ...");
  _fs.open(_path, std::ios::binary | std::ios::out);
  if (!_fs.good()) {
    log()->warn("writeHeader errno {} : {}", errno, _path.string());
    return false;
  }

  auto b_vid   = vid();
  auto b_vgid  = vgid();
  auto b_flags = newFlags.value_or(flags());
  auto b_dpi   = dpi();

  _fs.seekp(0);
  _fs << MAGIC_NUMBER_FILE;
  _fs.write(static_cast<char *>(static_cast<void *>(&b_vid)), sizeof(VolumeId));
  _fs.write(static_cast<char *>(static_cast<void *>(&b_vgid)), sizeof(VolumeGroupId));
  _fs.write(static_cast<char *>(static_cast<void *>(&b_flags)), sizeof(VolumeFlags));
  _fs.write(static_cast<char *>(static_cast<void *>(&b_dpi)), sizeof(DataProtectionIndex));

  _fs.seekp(VOLUME_HEADER_SIZE - 1);
  _fs << '\0';

  _fs.close();
  log()->info("Header written !");
  return true;
}

/**
 * Helper function to compare two magic numbers : one statically stored against one read from an istream
 * @tparam size the size of the magic number, only the first size bytes will be checked. The function DOES NOT check range !
 */
bool LocalVolume::checkMagicNumber(const std::array<char, MAGIC_NUMBERS_LENGTH> &magicArr, const std::string_view &magicStr) {
  for (size_t i = 0; i < MAGIC_NUMBERS_LENGTH; i++) {
    if (magicArr.at(i) != magicStr.at(i)) { return false; }
  }
  return true;
}

std::string LocalVolume::string() const {
  return fmt::format("Local Volume {:x} : vgid {:L} - Flags {:L} - DPI {:L} - at {}", vid(), vgid(), flags(), dpi(), _path.string());
}

/**
 * Creates a needle and update the index (RAM sync / .kidx async)
 * @param size of the needle's data
 * @return the FID and the cookie of the newly created Needle
 * @throws ERR_SPACE if the size doesn't fit in the remaining space
 * @throws ERR_DISK if the needle could not be added to the volume file
 */
std::pair<FileId, NeedleCookie> LocalVolume::createNeedle(NeedleSize size) {
  if (!hasEnoughSpace(size)) {
    log()->warn("Can't create a new needle of size {}", size);
    throw std::runtime_error("ERR_SPACE");
  }

  std::random_device                          rd;
  std::mt19937                                gen(rd());
  std::uniform_int_distribution<FileId>       rng64;
  FileId                                      fid = rng64(gen);
  std::uniform_int_distribution<NeedleCookie> rng32;
  NeedleCookie                                cookie = rng32(gen);

  while (_index.contains(fid)) {
    fid = rng64(gen);
  }

  auto [success, offset] = appendNeedle(fid, cookie, size, NeedleFlags::None);
  if (!success) {
    log()->warn("Could not append the needle on the disk");
    throw std::runtime_error("ERR_DISK");
  }

  _index.insert({ fid, IndexEntry(fid, cookie, size, offset, NeedleFlags::None) });
  // TODO update (async) .kidx file

  _fileSize = offset;

  return { fid, cookie };
}

/**
 * Append (only) a new Needle at the end of the Volume.
 * @param offset must be the current fileSize, sanity check
 * @return true if it has been able to create the needle in the volume
 * @throws ERR_CORRUPT_SIZE if the current offset doesn't match the file size
 * @throws ERR_CORRUPT_PREV if the previous needle's magic number was not found
 */
std::pair<bool, Offset> LocalVolume::appendNeedle(FileId fid, NeedleCookie cookie, NeedleSize size, NeedleFlags flags) {
  _fs.open(_path, std::ios::binary | std::ios::in | std::ios::out | std::ios::ate);
  if (!_fs.good()) {
    log()->warn("appendNeedle errno {} : {}", errno, _path.string());
    return { false, 0 };
  }

  // Check offset correctness
  auto currentOffset = static_cast<std::streamoff>(_fileSize);
  if (_fs.tellp() != currentOffset) {
    log()->critical("Prevented corruption due to invalid state : invalid file size");
    throw std::runtime_error("ERR_CORRUPT_SIZE");
  }

  // Check previous footer if any
  if (_fileSize != VOLUME_HEADER_SIZE) {
    _fs.seekg(currentOffset - NEEDLE_MAX_FOOTER_SIZE);
    std::array<char, NEEDLE_MAX_FOOTER_SIZE> previousFooter = {};
    _fs.read(previousFooter.data(), NEEDLE_MAX_FOOTER_SIZE);

    auto checkBytes = [&](size_t searchAt) {
      for (size_t j = 0; j < MAGIC_NUMBER_NEEDLE_END.size(); j++) {
        if (previousFooter.at(searchAt + j) != MAGIC_NUMBER_NEEDLE_END.at(j)) {
          return false;
        }
      }
      return true;
    };
    bool foundPreviousNeedle = false;
    for (size_t i = 0; i < previousFooter.size() - MAGIC_NUMBER_NEEDLE_END.size(); i++) {
      if (checkBytes(i)) {
        foundPreviousNeedle = true;
        break;
      }
    }
    if (!foundPreviousNeedle) {
      log()->critical("Prevented corruption due to invalid state : previous footer not found");
      throw std::runtime_error("ERR_CORRUPT_PREV");
    }
  }

  _fs.seekp(currentOffset);
  _fs << MAGIC_NUMBER_NEEDLE_START;
  _fs.write(static_cast<char *>(static_cast<void *>(&fid)), sizeof(FileId));
  _fs.write(static_cast<char *>(static_cast<void *>(&cookie)), sizeof(NeedleCookie));
  _fs.write(static_cast<char *>(static_cast<void *>(&size)), sizeof(NeedleSize));
  _fs.write(static_cast<char *>(static_cast<void *>(&flags)), sizeof(NeedleFlagsByte));
  // `size` bytes

  _fs.seekp(_fs.tellp() + static_cast<std::streamoff>(size));
  _fs << MAGIC_NUMBER_NEEDLE_END;
  // `NEEDLE_CHECKSUM_SIZE` bytes

  _fs.seekp(_fs.tellp() + NEEDLE_CHECKSUM_SIZE);
  std::array<char, BYTES_ALIGNMENT> padding{};
  padding.fill('\0');
  _fs.write(static_cast<char *>(static_cast<void *>(&padding)), COMPUTE_NEEDLE_PADDING(size));

  Offset finalOffset = static_cast<Offset>(_fs.tellp());
  _fs.close();
  log()->info("Needle with fid {:x}, volume's new filesize : 0x{:x}", fid, finalOffset);

  return { true, finalOffset };
}

/**
 * TODO Fix this non-working function
 */
bool LocalVolume::savePendingIndexEntries() {
  std::basic_fstream<char> fs(_indexPath, std::ios::binary | std::ios::out | std::ios::ate);
  if (static_cast<Offset>(fs.tellp()) == 0) {
    fs << MAGIC_NUMBER_INDEX;
    std::array<char, INDEX_HEADER_SIZE - MAGIC_NUMBER_INDEX.size()> indexPadding{};
    indexPadding.fill('\0');
    fs.write(indexPadding.data(), indexPadding.size());
  }

  if (!fs.good()) {
    log()->warn("savePendingIndexEntries errno {} : {}", errno, _indexPath.string());
    return false;
  }

  while (!_entriesToSave.empty()) {
    auto        fid    = _entriesToSave.back();
    IndexEntry &entry  = _index.at(fid);
    auto        cookie = entry.cookie();
    auto        size   = entry.size();
    auto        offset = entry.offset();
    auto        flags  = entry.flags();

    fs << MAGIC_NUMBER_ENTRY;
    fs.write(static_cast<char *>(static_cast<void *>(&fid)), sizeof(FileId));
    fs.write(static_cast<char *>(static_cast<void *>(&cookie)), sizeof(NeedleCookie));
    fs.write(static_cast<char *>(static_cast<void *>(&size)), sizeof(NeedleSize));
    fs.write(static_cast<char *>(static_cast<void *>(&offset)), sizeof(Offset));
    fs.write(static_cast<char *>(static_cast<void *>(&flags)), sizeof(NeedleFlagsByte));
    std::array<char, BYTES_ALIGNMENT - sizeof(NeedleFlags)> entryPadding{};
    entryPadding.fill('\0');
    fs.write(static_cast<char *>(static_cast<void *>(&entryPadding)), entryPadding.size());

    _entriesToSave.pop_back();
  }

  fs.close();
  return true;
}

bool LocalVolume::hasEnoughSpace(NeedleSize spaceSize) const {
  return (MAX_VOLUME_SIZE - _fileSize) >= NEEDLE_TOTAL_SPACE(spaceSize);
}

void LocalVolume::parseIndexFile() {
  log()->debug("Reading index file ...");
  std::basic_fstream<char>                   fs(_indexPath, std::ios::binary | std::ios::in | std::ios::ate);
  auto                                       fileSize = fs.tellg();
  fs.seekg(0);

  std::array<char, MAGIC_NUMBER_INDEX.size()> magicNumber{};
  fs.read(magicNumber.data(), MAGIC_NUMBER_INDEX.size());
  if (!checkMagicNumber(magicNumber, MAGIC_NUMBER_INDEX)) {
    log()->critical("Index file parsing error at offset 0x{:x} : MAGIC_NUMBER_INDEX not found",
      static_cast<Offset>(fs.tellg()) - MAGIC_NUMBER_INDEX.size());
    throw std::runtime_error("ERR_MAGIC");
  }
  magicNumber.fill('\x00');

  fs.seekg(INDEX_HEADER_SIZE);

  while (fs.tellg() != fileSize) {
    fs.read(magicNumber.data(), MAGIC_NUMBER_ENTRY.size());
    if (!checkMagicNumber(magicNumber, MAGIC_NUMBER_ENTRY)) {
      log()->critical("Index file parsing error at offset 0x{:x} : MAGIC_NUMBER_ENTRY not found",
        static_cast<Offset>(fs.tellg()) - MAGIC_NUMBER_ENTRY.size());
      throw std::runtime_error("ERR_MAGIC");
    }
    magicNumber.fill('\x00');

    FileId fid = 0;
    fs.read(static_cast<char *>(static_cast<void *>(&fid)), sizeof(FileId));

    NeedleCookie cookie = 0;
    fs.read(static_cast<char *>(static_cast<void *>(&cookie)), sizeof(NeedleCookie));

    NeedleSize size = 0;
    fs.read(static_cast<char *>(static_cast<void *>(&size)), sizeof(NeedleSize));

    Offset needleOffset = 0;
    fs.read(static_cast<char *>(static_cast<void *>(&needleOffset)), sizeof(Offset));

    NeedleFlags flags = NeedleFlags::None;
    fs.read(static_cast<char *>(static_cast<void *>(&flags)), sizeof(NeedleFlagsByte));
    fs.seekg(fs.tellg() + static_cast<std::streamoff>(BYTES_ALIGNMENT - sizeof(NeedleFlags)));

    IndexEntry entry{ fid, cookie, size, needleOffset, flags };
    _index.emplace(fid, entry);
  }

  fs.close();
  log()->debug("Found {} needles after index parsing", _index.size());
}

void LocalVolume::rebuildIndex() {
  log()->info("Rebuilding index ...");
  _fs.open(_path, std::ios::binary | std::ios::in | std::ios::ate);
  auto fileSize = _fs.tellg();
  _fs.seekg(VOLUME_HEADER_SIZE);

  while (_fs.tellg() != fileSize) {
    Offset                                     needleOffset = static_cast<Offset>(_fs.tellg());

    std::array<char, MAGIC_NUMBER_FILE.size()> magicNumber{};
    _fs.read(magicNumber.data(), MAGIC_NUMBER_FILE.size());
    if (!checkMagicNumber(magicNumber, MAGIC_NUMBER_NEEDLE_START)) {
      log()->critical("Volume file parsing error at offset 0x{:x} : MAGIC_NUMBER_NEEDLE_START not found",
        static_cast<Offset>(_fs.tellg()) - MAGIC_NUMBER_NEEDLE_START.size());
      throw std::runtime_error("ERR_MAGIC");
    }
    magicNumber.fill('\x00');

    FileId fid = 0;
    _fs.read(static_cast<char *>(static_cast<void *>(&fid)), sizeof(FileId));

    NeedleCookie cookie = 0;
    _fs.read(static_cast<char *>(static_cast<void *>(&cookie)), sizeof(NeedleCookie));

    NeedleSize size = 0;
    _fs.read(static_cast<char *>(static_cast<void *>(&size)), sizeof(NeedleSize));

    NeedleFlags flags = NeedleFlags::None;
    _fs.read(static_cast<char *>(static_cast<void *>(&flags)), sizeof(NeedleFlagsByte));

    // DATA
    _fs.seekg(_fs.tellg() + static_cast<std::streamoff>(size));

    _fs.read(magicNumber.data(), MAGIC_NUMBER_FILE.size());
    if (!checkMagicNumber(magicNumber, MAGIC_NUMBER_NEEDLE_END)) {
      log()->critical("Volume file parsing error at offset 0x{:x} : MAGIC_NUMBER_NEEDLE_END not found",
        static_cast<Offset>(_fs.tellg()) - MAGIC_NUMBER_NEEDLE_END.size());
      throw std::runtime_error("ERR_MAGIC");
    }

    // TODO Store checksum
    _fs.seekg(_fs.tellg() + static_cast<std::streamoff>(NEEDLE_CHECKSUM_SIZE));

    IndexEntry entry{ fid, cookie, size, needleOffset, flags };
    _index.emplace(fid, entry);
    _entriesToSave.emplace_back(fid);

    // PADDING
    _fs.seekg(_fs.tellg() + static_cast<std::streamoff>(COMPUTE_NEEDLE_PADDING(size)));
  }

  _fileSize = static_cast<Offset>(_fs.tellg());
  _fs.close();

  if (exists(_indexPath)) {
    std::filesystem::remove(_indexPath);
  }
  savePendingIndexEntries();

  log()->debug("Found {} needles after index rebuild", _index.size());
}
