//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_DISK_HPP
#define KARNET_DISK_HPP

#include <string>
#include <filesystem>
#include <spdlog/spdlog.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include "../data-structures/Volume.hpp"

class Disk
{
public:
  explicit Disk(std::string name);
  Disk(const Disk &) = delete;
  Disk(Disk &&)      = default;

  [[nodiscard]] const std::string &name() const { return _name; }
  [[nodiscard]] DiskType           type() const { return _type; }

  /* Public static helper methods */
  static DiskType                  stringToDiskType(const std::string &str);
  static std::string               diskTypeToString(DiskType diskType);

protected:
  [[nodiscard]] auto log() const { return _log; }

private:
  std::shared_ptr<spdlog::logger> _log;
  const std::string               _name;
  const DiskType                  _type;

  static DiskType                 diskTypeFromName(const std::string &name);
};

class LocalDisk : public Disk
{
public:
  explicit LocalDisk(std::string name, std::filesystem::path path);
  using constVolumeIterator = std::unordered_map<VolumeId, LocalVolume>::const_iterator;

  bool                                createVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi);
  std::pair<FileId, NeedleCookie>     createNeedle(VolumeId vid, NeedleSize size);

  [[nodiscard]] std::uintmax_t        availableSpace() const { return _availableSpace; }
  [[nodiscard]] const LocalVolume    &volume(VolumeId vid) const { return _volumes.at(vid); }
  [[nodiscard]] std::vector<VolumeId> volumeIds() const;
  [[nodiscard]] constVolumeIterator   volumesBegin() const { return _volumes.begin(); };
  [[nodiscard]] constVolumeIterator   volumesEnd() const { return _volumes.end(); };
  [[nodiscard]] bool                  hasVolume(VolumeId vid) const { return _volumes.contains(vid); }
  [[nodiscard]] bool                  hasEnoughSpace(NeedleSize size) const { return _availableSpace >= size; }

private:
  std::filesystem::path                     _path;
  std::uintmax_t                            _availableSpace;
  std::uintmax_t                            _usedSpace;
  std::uintmax_t                            _diskCapacity;
  size_t                                    _percent;
  std::unordered_map<VolumeId, LocalVolume> _volumes;
};


#endif// KARNET_DISK_HPP
