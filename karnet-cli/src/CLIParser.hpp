//
// Created by loggerz on 06/09/2021.
//

#ifndef KARNET_CLIPARSER_HPP
#define KARNET_CLIPARSER_HPP

#include <map>
#include <string>
#include <docopt/docopt.h>
#include "../../karnet-core/lib/local-storage/LocalStorage.hpp"


class CLIParser
{
public:
  explicit CLIParser(const std::map<std::basic_string<char>, docopt::value> &args, LocalStorage &ls);

private:
};


#endif//KARNET_CLIPARSER_HPP
