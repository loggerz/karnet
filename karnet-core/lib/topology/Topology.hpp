//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_TOPOLOGY_HPP
#define KARNET_TOPOLOGY_HPP

#include <vector>

#include "DataCenter.hpp"

enum class KarnetRole {
  // A client doesn't store volumes
  Client,
  // A workstation stores volumes that may be shared
  Workstation,
  // An IG is responsible for a Datacenter and is a StorageNode
  InternalGateway,
  // An EG acts as a StorageNode exposed to the Internet
  ExternalGateway,
  // A StorageNode hosts volumes for local machines
  StorageNode,
  // Undefined role, disabled machine or pending configuration
  Unknown
};

class Topology
{
public:
private:
  std::vector<DataCenter> _dataCenters;
};


#endif//KARNET_TOPOLOGY_HPP
