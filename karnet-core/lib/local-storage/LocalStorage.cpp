//
// Created by loggerz on 20/08/2021.
//

#include <spdlog/sinks/stdout_color_sinks.h>
#include <fstream>
#include <random>
#include <unordered_set>

#include "LocalStorage.hpp"

LocalStorage::LocalStorage() : _ready(false), _log(spdlog::stdout_color_mt("LocalStorage")) {
  _log->set_level(spdlog::level::debug);

#ifdef _WIN32
#error Platform not supported yet
  auto userAppdataPath{ "~/.config" };
#elif __APPLE__
#error Platform not supported yet
  std::string userAppdataPath = getenv("HOME");
  userAppdataPath += "~/.config";
#else
  std::string userAppdataPath = getenv("HOME");
  userAppdataPath += "/.config";
#endif

  auto path = std::filesystem::path(userAppdataPath);
  if (!std::filesystem::directory_entry(path).exists()) {
    _log->critical("User appdata path {} is invalid, platform not supported yet.");
    throw std::runtime_error("invalid user appdata path");
  }

  path += "/karnet";
  _karnetConfigFolder = std::filesystem::directory_entry(path);
  if (!_karnetConfigFolder.exists()) {
    _log->info("No config folder found, creating it at {}", path.string());
    std::filesystem::create_directory(_karnetConfigFolder);
  } else {
    _log->info("Loading configuration from {}", path.string());
  }

  auto jsonFilePath = std::filesystem::path{ _karnetConfigFolder.path() };
  jsonFilePath += "/config.json";

  _config = std::make_shared<Configuration>(Configuration(jsonFilePath));
  if (!_config->isValid()) { return; }

  if (!prepareDisks()) {
    _log->warn("Unable to prepare some volume roots");
    return;
  }

  _ready = true;
}

bool LocalStorage::prepareDisks() {
  std::unordered_set<std::string> names;
  for (const auto &diskKV : _config->disks()) {
    const std::filesystem::directory_entry dir{ *diskKV.second };
    if (!dir.exists()) {
      _log->info("Disk {} doesn't exist, creating it ...", diskKV.first);
      if (!std::filesystem::create_directory(dir)) { return false; }
    } else {
      _log->info("Indexing disk {} ...", diskKV.first);
      auto [_, unique] = names.insert(diskKV.first);
      if (!unique) {
        _log->warn("Duplicate disk name {}", diskKV.first);
        return false;
      }
    }
    LocalDisk disk(diskKV.first, *diskKV.second);
    _disks.emplace(disk.name(), std::move(disk));
  }
  return true;
}

bool LocalStorage::createVolume(VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi) {
  std::random_device                      rd;
  std::mt19937                            gen(rd());
  std::uniform_int_distribution<VolumeId> rng;
  VolumeId                                vid = rng(gen);
  _log->debug("Creating new volume {:x}", vid);

  // TODO: More rules to choose disks

  if (_disks.empty()) {
    _log->warn("No disk available !");
    return false;
  }

  // Choose the disk with the most available space
  const auto diskItr = std::max_element(_disks.begin(), _disks.end(), [](const auto &i, const auto &j) {
    return i.second.availableSpace() < j.second.availableSpace();
  });

  if (diskItr->second.availableSpace() < MAX_VOLUME_SIZE) {
    _log->warn("Creating a 32GiB volume on a disk with only {:L} bytes left !", diskItr->second.availableSpace());
  }

  return diskItr->second.createVolume(vid, vgid, flags, dpi);
}

std::vector<std::pair<VolumeId, std::string>> LocalStorage::getAllLocalVolumeIds() const {
  std::vector<std::pair<VolumeId, std::string>> volumeIds{};
  for (const auto &diskKV : _disks) {
    for (auto itr = diskKV.second.volumesBegin(); itr != diskKV.second.volumesEnd(); itr++) {
      volumeIds.emplace_back(itr->second.vid(), diskKV.first);
    }
  }
  return volumeIds;
}

LocalDisk &LocalStorage::lookupDisk(VolumeId vid) {
  try {
    const auto diskName = _volumesLUT.at(vid);
    return _disks.at(diskName);
  } catch (const std::out_of_range &) {
    for (auto &diskKV : _disks) {
      if (diskKV.second.hasVolume(vid)) {
        _volumesLUT.emplace(vid, diskKV.first);
        return diskKV.second;
      }
    }
    throw std::out_of_range("Volume not found");
  }
}

/**
 * Search a volume among all local disks
 * @throws std::out_of_range if not found
 */
const LocalVolume &LocalStorage::findVolume(VolumeId vid) {
  return lookupDisk(vid).volume(vid);
}

std::pair<FileId, NeedleCookie> LocalStorage::reserveSpace(VolumeId vid, NeedleSize spaceSize) {
  auto       &disk   = lookupDisk(vid);
  const auto &volume = disk.volume(vid);
  if (!disk.hasEnoughSpace(spaceSize) || !volume.hasEnoughSpace(spaceSize)) {
    throw std::overflow_error(fmt::format("Not enough space, disk:{} volume:{}",disk.hasEnoughSpace(spaceSize),volume.hasEnoughSpace(spaceSize)));
  }
  auto [fid, cookie] = disk.createNeedle(vid, spaceSize);
  return { fid, cookie };
}
