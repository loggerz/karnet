//
// Created by loggerz on 20/08/2021.
//

#include <utility>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "Disk.hpp"

/* DISK */

Disk::Disk(std::string name) : _name(std::move(name)), _type(diskTypeFromName(_name)) {
  _log = spdlog::stdout_color_mt(fmt::format("Disk {}", _name));
  _log->set_level(spdlog::level::debug);
}

DiskType Disk::diskTypeFromName(const std::string &name) {
  auto underscoreIndex = name.find('_');
  if (underscoreIndex != std::string::npos) {
    return stringToDiskType(name.substr(0, underscoreIndex));
  }
  return DiskType::Unknown;
}

/* LOCAL DISK */

LocalDisk::LocalDisk(std::string name, std::filesystem::path path) : Disk(std::move(name)), _path(std::move(path)), _usedSpace(0) {
  auto diskStats  = std::filesystem::space(_path);
  _availableSpace = diskStats.available;                                                                                       // NOLINT : can't use member initializer
  _diskCapacity   = diskStats.capacity;                                                                                        // NOLINT
  _percent        = 100 - static_cast<size_t>((static_cast<float>(_availableSpace)) / static_cast<float>(_diskCapacity) * 100);// NOLINT : no constant for percentages

  if (_availableSpace <= MAX_VOLUME_SIZE) {
    log()->warn("Disk has less than 32GiB of available space ! Currently at {:d}% of capacity : {:L} / {:L}", _percent, _diskCapacity - _availableSpace, _diskCapacity);
  }

  for (const auto &entry : std::filesystem::directory_iterator(_path)) {
    if (entry.is_regular_file() && entry.path().string().ends_with(FILE_EXTENSION_VOLUME)) {
      LocalVolume vol = LocalVolume::loadFromHeader(entry.path());
      const auto  vid = vol.vid();
      _usedSpace += vol.size();
      _volumes.emplace(vid, std::move(vol));
    }
  }

  if (_volumes.empty()) {
    log()->warn("No volume found on root {}", _path.string());
  }
}

bool LocalDisk::createVolume(VolumeId vid, VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi) {
  try {
    auto lv = LocalVolume::createNewVolume(vid, vgid, flags, dpi, _path);
    _volumes.emplace(vid, std::move(lv));
    return true;
  } catch (const std::runtime_error &err) {
    log()->critical(err.what());
    return false;
  }
}

std::pair<FileId, NeedleCookie> LocalDisk::createNeedle(VolumeId vid, NeedleSize size) {
  auto [fid, cookie] = _volumes.at(vid).createNeedle(size);
  _usedSpace += size;
  return {fid, cookie};
}

/**
 * @return a newly constructed vector of Volumes' VIDs
 */
std::vector<VolumeId> LocalDisk::volumeIds() const {
  std::vector<VolumeId> ids;
  ids.reserve(_volumes.size());
  for (const auto &kv : _volumes) {
    ids.push_back(kv.first);
  }
  return ids;
}

/* STATIC HELPERS */

DiskType Disk::stringToDiskType(const std::string &str) {
  if (str == "ram") { return DiskType::RAM; }
  if (str == "nvme") { return DiskType::NVMe; }
  if (str == "ssd") { return DiskType::SSD; }
  if (str == "hdd") { return DiskType::HDD; }
  if (str == "archive") { return DiskType::Archive; }
  return DiskType::Unknown;
}

// std::string Disk::diskTypeToString(DiskType diskType) {
//   switch (diskType) {
//   case DiskType::RAM: return "ram";
//   case DiskType::NVMe: return "nvme";
//   case DiskType::SSD: return "ssd";
//   case DiskType::HDD: return "hdd";
//   case DiskType::Archive: return "archive";
//   default: return "unknown";
//   }
// }