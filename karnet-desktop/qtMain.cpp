#include <QApplication>

#include "../karnet-core/lib/myTestClass.hpp"

#include "HelloQt.hpp"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  HelloQt      dialog;
  dialog.show();

  // test call to karnet-core
  auto test = MyTestClass{};
  test.log();

  return QApplication::exec();// this runs the main event loop and sees to it that cleanup is done
}
