#include <functional>
#include <iostream>
#include <fstream>
#include <string>
#include <array>
#include <algorithm>
#include <algorithm>

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <spdlog/fmt/bin_to_hex.h>
#include <docopt/docopt.h>

#include "../../karnet-core/lib/local-storage/LocalStorage.hpp"
#include "CLIParser.hpp"

static constexpr auto USAGE =
  R"(Karnet CLI Tool

    Usage:
          karnet volume list
          karnet volume create
          karnet volume <vid> info
          karnet volume <vid> needle create <size>
          karnet volume <vid> index
          karnet dev <fn>
          karnet (-h | --help)
          karnet --version
 Options:
          -h --help     Show this screen.
          --version     Show version.
)";

int main([[maybe_unused]] int argc, [[maybe_unused]] const char **argv) {

  std::map<std::string, docopt::value> args = docopt::docopt(USAGE,
    { std::next(argv), std::next(argv, argc) },
    true,             // show help if requested
    "Karnet CLI 0.1");// version string

    for (auto const &arg : args) {
      std::cout << arg.first << "  —  " << arg.second << std::endl;
    }

  LocalStorage                         ls;
  CLIParser                            parser{ args, ls };
}
