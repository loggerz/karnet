//
// Created by loggerz on 20/08/2021.
//

#ifndef KARNET_LOCALSTORAGE_HPP
#define KARNET_LOCALSTORAGE_HPP

#include <filesystem>
#include <spdlog/spdlog.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <json/json.h>

#include "../Constants.hpp"
#include "../Configuration.hpp"

/**
 * The entry point for every operation on local storage.
 * Will call encryption routines for data storage, store the standard
 * paths for configurations files etc.
 */
class LocalStorage
{
public:
  LocalStorage();

  [[nodiscard]] bool                                          isReady() const { return _ready; }

  /* Volume storage */
  bool                                                        createVolume(VolumeGroupId vgid, VolumeFlags flags, DataProtectionIndex dpi);
  [[nodiscard]] std::vector<std::pair<VolumeId, std::string>> getAllLocalVolumeIds() const;
  [[nodiscard]] const LocalVolume                            &findVolume(VolumeId vid);
  std::pair<FileId, NeedleCookie>                             reserveSpace(VolumeId vid, NeedleSize spaceSize);

  /* Cache storage */

  /* Local filesystem storage */

private:
  bool                                       _ready;
  std::shared_ptr<spdlog::logger>            _log;
  std::filesystem::directory_entry           _karnetConfigFolder;
  std::shared_ptr<Configuration>             _config;
  std::unordered_map<std::string, LocalDisk> _disks;
  std::unordered_map<VolumeId, std::string>  _volumesLUT;

  bool                                       prepareDisks();
  LocalDisk                                 &lookupDisk(VolumeId vid);
};


#endif// KARNET_LOCALSTORAGE_HPP
