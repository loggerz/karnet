
set(CORE_INCLUDES
        lib/data-structures/IndexEntry.hpp
        lib/data-structures/Volume.cpp
        lib/data-structures/Volume.hpp
        lib/topology/DataCenter.cpp
        lib/topology/DataCenter.hpp
        lib/topology/DataNode.cpp
        lib/topology/DataNode.hpp
        lib/topology/MasterNode.cpp
        lib/topology/MasterNode.hpp
        lib/topology/Disk.cpp
        lib/topology/Disk.hpp
        lib/topology/Topology.cpp
        lib/topology/Topology.hpp
        lib/Constants.hpp
        lib/local-storage/LocalStorage.cpp
        lib/local-storage/LocalStorage.hpp
        lib/util/Logger.cpp
        lib/util/Logger.hpp lib/Configuration.cpp lib/Configuration.hpp lib/data-structures/VolumeGroup.cpp lib/data-structures/VolumeGroup.hpp lib/data-structures/Identity.cpp lib/data-structures/Identity.hpp lib/operations/CreateNeedle.cpp lib/operations/CreateNeedle.hpp lib/operations/UserSignedOperation.cpp lib/operations/UserSignedOperation.hpp)

find_package(CryptoPP CONFIG REQUIRED)

add_library(KarnetCore ${CORE_INCLUDES})
target_link_libraries(KarnetCore
        PRIVATE project_options project_warnings
        CONAN_PKG::fmt
        CONAN_PKG::spdlog
        CONAN_PKG::jsoncpp
        CONAN_PKG::asio
        cryptopp-static)

message("KARNET-CORE CONFIGURED")